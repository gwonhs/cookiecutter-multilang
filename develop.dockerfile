FROM debian:bullseye-slim

SHELL ["/bin/bash", "-l", "-c"]

RUN apt-get update && apt-get install -y curl


# Python

RUN curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && chmod +x Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh

ENV PATH /root/miniconda3/bin:$PATH
RUN /root/miniconda3/bin/pip install pytest behave flake8


# Node.js

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
RUN nvm install node
RUN npm config set user 0
RUN npm config set unsafe-perm true

