{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}

.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_name }}.svg
        :target: https://pypi.python.org/pypi/{{ cookiecutter.project_name }}

.. image:: https://gitlab.com/gwonhs/timeframe/badges/master/pipeline.svg
     :target: https://gitlab.com/gwonhs/timeframe/-/commits/master

.. image:: https://gitlab.com/gwonhs/timeframe/badges/master/coverage.svg
     :target: https://gitlab.com/gwonhs/timeframe/-/commits/master



{{ cookiecutter.project_description }}

* Free software: MIT License

Features
--------

* TODO
