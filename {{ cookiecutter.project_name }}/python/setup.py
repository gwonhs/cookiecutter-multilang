#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('../README.rst') as readme_file:
    readme = readme_file.read()

with open('../HISTORY.rst') as history_file:
    history = history_file.read()

requirements = []

setup_requirements = ['pytest-runner']

test_requirements = ['pytest>=3']


setup(
    author="{{ cookiecutter.author.replace('\"', '\\\"') }}",
    author_email='{{ cookiecutter.email }}',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="{{ cookiecutter.project_description }}",
    {%- if cookiecutter.python_cli == 'y' %}
    entry_points={
        'console_scripts': [
            '{{ cookiecutter.project_name }}={{ cookiecutter.project_name }}.cli:main',
        ],
    },
    {%- endif %}
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='{{ cookiecutter.project_name }}',
    name='{{ cookiecutter.project_name }}',
    packages=find_packages(include=['{{ cookiecutter.project_name }}', '{{ cookiecutter.project_name }}.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_name }}',
    version='{{ cookiecutter.version }}',
    zip_safe=False,
)