#!/usr/bin/env python
import os
import shutil
from pathlib import Path

PROJECT_DIRECTORY = Path(os.path.realpath(os.path.curdir))


if __name__ == '__main__':

    if '{{ cookiecutter.python }}' == 'n':
        shutil.rmtree(PROJECT_DIRECTORY / 'python')
