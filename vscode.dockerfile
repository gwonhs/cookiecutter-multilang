FROM gwonhs/develop

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID


# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME


# Create directory for vscode-container
RUN mkdir -p /home/$USERNAME/.vscode-server \
        /home/$USERNAME/.vscode-server-insiders \
    && chown -R $USERNAME \
        /home/$USERNAME/.vscode-server \
        /home/$USERNAME/.vscode-server-insiders


RUN apt update && apt install -y \
    git vim


RUN mkdir -p /workspace && chown -R $USERNAME:$USERNAME /workspace

USER vscode

WORKDIR /workspace

# ENV PATH="/home/vscode/miniconda3/bin:$PATH"
